/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package libraryproject;

import database.Database;
import database.User;
import database.UserDao;
import java.sql.Connection;

/**
 *
 * @author hafish
 */
public class LibraryProject {

    static Connection conn = null;

    public static void main(String[] args) {

//        User newUser = new User();
//        newUser.setUserId(-1);
//        newUser.setLoginName("Honda");
//        newUser.setPassword("Hon12345678");
//        newUser.setName("Gear");
//        newUser.setSurname("PA");
//        newUser.setTypeId(1);
//        UserDao.insert(newUser);
//        UserDao.select();
        int userId = 1;
        User user = UserDao.getUser(userId);
        if (user == null) {
            System.out.println("Can't find User : " + userId);
        } else {
            System.out.println(user);
        }
        UserDao.delete(user);
        
        user = UserDao.getUser(userId);
        if (user == null) {
            System.out.println("Can't find User : " + userId);
        } else {
            System.out.println(user);
        }

    }
}
